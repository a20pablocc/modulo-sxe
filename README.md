Mi modulo consiste en un gestor de plantillas de equipos de fútbol en el cual se pueden ver las estadísticas de los jugadores. Está enfocado para el entrenador del equipo y así poder ayudarlo a realizar el 11 inicial y conseguir mejores resultados.

Está compuesto por tres vistas:

La vista de estadísticas de jugadores donde se pueden ver partidos jugados, goles, asistencias, tarjetas e incluso los goles que lleva por partido esta vista contiene una vista gráfico donde de forma visual se pueden ver las estadísticas de todos los jugadores y así saber quienes están rindiendo mejor o peor.

La vista jugadores personal contiene los datos personales de cada jugador, peso, altura, dirección, teléfono.

Por último está la vista jugadores titulares, que hereda de la de jugadores personal, para poder así escoger a los jugadores ya creados anteriomente. En la vista podemos seleccionar que jugadores jugarán el partido seleccionado, con la fecha, el rival y la posición en la que jugarán, esto se puede realizar facilmente con la vista gráfico que nos dice que jugadores rinden mejor para ser titulares.

Aquí podemos ver también una vista kanban ordenada por la posición en la que jugará ese jugador y permite cambiar la posición.

Por último existe una vista calendario que nos muestra cuando van a ser los partidos de los jugadores que van a ser titulares.

Como posibles mejoras futuras he pensado:

- La posibilidad de límitar a 11 los jugadores que van a ser introducidos en el modelo de titulares.
- Crear un historial de partidos y que jugadores fueron titulares en eses partidos.
- Eliminar los jugadores del modelo titulares una vez pasada la fecha del partido e introducirlos en ese modelo historial de equipos.
