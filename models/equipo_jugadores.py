# -*- coding: utf-8 -*-
import logging
from odoo.exceptions import UserError
from odoo import models, fields, api

_logger = logging.getLogger(__name__)

class equipo_jugadores(models.Model):
    _name = 'equipo.jugadores'
    _description = 'Estadísticas jugadores'

    name_id = fields.Many2one('jugador.personal', String="Nombre",required=True)
    position = fields.Selection([
        ('PO', 'Portero'),
        ('LI', 'Lateral izquierdo'),
        ('DFC', 'Defensa central'),
        ('LD', 'Lateral derecho'),
        ('MCD', 'Medio centro defensivo'),
        ('MC', 'Medio centro'),
        ('MCO', 'Medio centro ofensivo'),
        ('EI', 'Extremo izquierdo'),
        ('DC', 'Delantero centro'),
        ('ED', 'Extremo derecho')],
        'Posición', default="PO")
    minutes = fields.Integer('Minutos jugados')
    goals = fields.Integer('Goles')
    assists = fields.Integer('Asistencias')
    amarillas = fields.Integer('Tarjetas amarillas')
    partidos = fields.Integer('Partidos jugados')
    rojas = fields.Integer('Tarjetas rojas')
    lesionado = fields.Boolean('Lesionado')
    lesionTime = fields.Date('Tiempo límite de lesión')
    goles_partido = fields.Float('Goles por partido', compute="_calcular_goles_minuto")

    @api.depends('goles_partido')
    def _calcular_goles_minuto(self):
        for jugador in self:
            if jugador.partidos == 0:
                jugador.goles_partido=0
            else:
                jugador.goles_partido = jugador.goals/jugador.partidos


    class jugador_personal(models.Model):
        _name = 'jugador.personal'

        name = fields.Char('Nombre')
        apellidos = fields.Char('Apellidos')
        mote = fields.Char('Mote')
        peso = fields.Float('Peso')
        altura = fields.Float('Altura')
        direccion = fields.Char('Direccion')
        email = fields.Char('Email')
        telefono = fields.Integer('Teléfono')
        foto = fields.Binary('Foto', attachment=True)

class jugador_titular(models.Model):
    _name='jugador.titular'
    _inherit = 'jugador.personal'

    name_titular= fields.Many2one('jugador.personal', String='Nombre', required='true')
    diaPartido = fields.Date('Día del partido')
    rival = fields.Selection([
        ('COR', 'Coruxo'),
        ('CELB', 'Celta B'),
        ('ZAM', 'Zamora'),
        ('UNI', 'Unionistas de Salamanca'),
        ('COM', 'Compostela'),
        ('RCF', 'Racing de Ferrol'),
        ('SAL', 'UDS Salamanca'),
        ('GUI', 'Guijuelo'),
        ('PON', 'Pontevedra')],
        'Rival', default="CELB")
    posicionPartido = fields.Selection([
        ('PO', 'Portero'),
        ('LI', 'Lateral izquierdo'),
        ('DFC', 'Defensa central'),
        ('LD', 'Lateral derecho'),
        ('MCD', 'Medio centro defensivo'),
        ('MC', 'Medio centro'),
        ('MCO', 'Medio centro ofensivo'),
        ('EI', 'Extremo izquierdo'),
        ('DC', 'Delantero centro'),
        ('ED', 'Extremo derecho')],
        'Posición', default="PO")


